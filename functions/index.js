const functions = require('firebase-functions');
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

//const MongoClient = require('mongodb').MongoClient;
//const uri = "mongodb+srv://pgsoto:toor189@cluster0-scxlm.gcp.mongodb.net/admin?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });

const { username, password } = functions.config().mongo

const uri = `mongodb://${username}:${password}@ds061474.mlab.com:61474/api-rest`;

const mongooseConfig = { useNewUrlParser: true }

mongoose.connect(uri, mongooseConfig)

const app = express()

const Pets = require('./Pets')

const createServer = () => {
    app.use(cors({ origin: true }))

    app.get('/pets', async(req, res) => {
        const result = await Pets.find({}).exec()
        res.send(result)
    })

    app.post('/pets', async(req, res) => {
        const { body } = req
        const pet = new Pets(JSON.parse(body))
        await pet.save()
        res.send(pet._id)
    })

    app.get('/pets/:id/daralta', async(req, res) => {
        const { id } = req.params
        await Pets.deleteOne({ _id: id }).exec()
        res.send(204)
    })

    return app
}

exports.api = functions.https.onRequest(createServer())